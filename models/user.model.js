const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const beautifyUnique = require("mongoose-beautiful-unique-validation");
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const persianPhoneRegex = /^09\d{9}$/;

const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		trim: true,
		required: false,
		minLength: 2,
		default: null
	},
	email: {
		type: String,
		trim: true,
		required: true,
		match: emailRegex,
		unique: true,
	},
	username: {
		type: String,
		trim: true,
		required: true,
		minLength: 3,
		unique: true,
	},
	phone: {
		type: String,
		required: true,
		match: persianPhoneRegex,
		unique: true
	},
});
UserSchema.plugin(beautifyUnique);
module.exports = mongoose.model("User", UserSchema);
