"use strict";
const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const ObjectId = require("mongoose").Types.ObjectId;
const {ValidationError, MoleculerError} = require("moleculer").Errors;
const errorHandler = require("./error.handler");

const User = require("./models/user.model");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const mongoUrl = (process.env.MONGO_URI || "mongodb://localhost:27017/") + "badesabaa";

module.exports = {
	name: "user",
	/**
	 * for more info on what actions can be used, see https://moleculer.services/docs/0.13/moleculer-db.html#Actions
	 */
	mixins: [DbService],
	adapter: new MongooseAdapter(mongoUrl),
	model: User,
	actions: {
		/**
		 * updates entity in db without using the DbService mixins
		 */
		customUpdate: {
			params: {
				id: {type: "string"}, // id is required and should be given
				email: {
					type: "email",
					optional: true
				},
				username: {
					type: "string",
					optional: true,
					min: 3
				},
				phone: {
					type: "string",
					pattern: /^09\d{9}$/, // persian phone regex
					optional: true
				},
				name: {
					type: "string",
					optional: true,
					min: 3,
				}
			},
			/**
			 * validates id to be a valid ObjectId then call updateUser function with given params to update the entity in db
			 * @param ctx
			 * @returns {Promise<User>}
			 */
			async handler(ctx) {
				// validate id to be a valid ObjectId so further down the line we do not encounter and error
				if (!ObjectId.isValid(ctx.params.id)) {
					throw new ValidationError("id is not a valid ObjectId", "id", {
						type: "id",
						message: "id is not a valid ObjectId",
						actual: ctx.params.id,
					});
				}
				return this.updateUser(ctx.params.id, ctx.params);
			}
		},


		/**
		 * just to showcase how can we call other actions inside an action
		 * also have more validation that user.update (params are defined)
		 */
		elegantUpdate: {
			params: {
				id: {type: "string"}, // id is required and should be given
				email: {
					type: "email",
					optional: true
				},
				username: {
					type: "string",
					optional: true,
					min: 3
				},
				phone: {
					type: "string",
					pattern: /^09\d{9}$/, // persian phone regex
					optional: true
				},
				name: {
					type: "string",
					optional: true,
					min: 3,
				}
			},
			async handler(ctx) {
				return await ctx.call("user.update", ctx.params);
			}
		}
	},
	methods: {
		/**
		 * tries to update a user given an id,
		 * can throw MoleculerError if entity not found or validationError if updating does not preserve uniqueness of a given field
		 * @param {String} _id
		 * @param {object} params
		 * @param {String} [params.email]
		 * @param {String} [params.name]
		 * @param {String} [params.username]
		 * @param {String} [params.phone]
		 * @returns {Promise<User>}
		 */
		async updateUser(_id, params) {
			try {
				const updatedUser = await User.findOneAndUpdate({_id}, params, {new: true}).exec();
				if (!updatedUser) {
					throw new MoleculerError("Entity Not found", 404, "EntityNotFound", {
						id: _id
					});
				}
			} catch (e) {
				errorHandler(e, params);
			}
		}
	}
};
