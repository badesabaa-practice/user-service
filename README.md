[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# Users Service
This is a [Moleculer](https://moleculer.services/)-based microservice. Generated with the [Moleculer CLI](https://moleculer.services/docs/0.14/moleculer-cli.html).  
Perform CRUD requests for user data.

## Prerequisites
```
npm install
```
This projects needs mongodb installed, change port and collection name in `service.js` if needed.  
Make sure [NATS server](https://nats.io/download/nats-io/nats-server/) is running before starting the project.  

## Usage
run the service by  `npm run dev` to run it as REPL mode. for more info on REPL commands see [here](https://moleculer.services/docs/0.12/moleculer-repl.html#REPL-Commands).  
To read more about available actions for user service, take a look at [Moleculer Db Adapters actions](https://moleculer.services/docs/0.13/moleculer-db.html#Actions).  

## Docker
Builad docker image  
```docker build -t user-service .```


## Useful links

* Moleculer website: https://moleculer.services/
* Moleculer Documentation: https://moleculer.services/docs/0.14/



## NPM scripts

- `npm run dev`: Start development mode (with hot-reload & REPL)
- `npm run start`: Start production mode
- `npm run lint`: Run ESLint
- `npm run ci`: Run continuous test mode with watching
