const {ValidationError} = require("moleculer").Errors;
const MongooseError = require("mongoose").Error;

module.exports = (e, params) => {
	if (e instanceof MongooseError.ValidationError) {
		/**
		 * if error is of kind of validationError caused by unique validator,
		 * it has an object errors and its keys are parameters with the problem
		 */
		if (e.errors) {
			const keys = Object.keys(e.errors);
			const errorMessage = `${keys[0]} is not a unique value`;
			throw new ValidationError(errorMessage, "id", {
				type: keys[0],
				message: errorMessage,
				actual: params[keys[0]],
			});
		}
	}
	/**
	 * if error is not type of MongooseError.ValidationError throw it
	 */
	throw e;
};
